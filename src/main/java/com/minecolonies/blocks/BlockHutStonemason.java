package com.minecolonies.blocks;

public class BlockHutStonemason extends BlockHut
{
    protected BlockHutStonemason()
    {
        super();
    }

    @Override
    public String getName()
    {
        return "blockHutStonemason";
    }
}